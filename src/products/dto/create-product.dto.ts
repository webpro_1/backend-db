import { IsNotEmpty, IsNumber, IsPositive, MinLength } from 'class-validator';

export class CreateProductDto {
  @IsNotEmpty()
  @MinLength(3)
  name: string;
  @IsNotEmpty()
  @IsNumber()
  @IsPositive()
  price: number;
}
