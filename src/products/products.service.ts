import { Injectable } from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product } from './entities/product.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class ProductsService {
  constructor(
    @InjectRepository(Product) private productRepository: Repository<Product>,
  ) {}
  create(createProductDto: CreateProductDto) {
    const createProduct = new Product();
    createProduct.name = createProductDto.name;
    createProduct.price = createProductDto.price;
    return this.productRepository.save(createProduct);
  }

  findAll() {
    return this.productRepository.find();
  }

  findOne(id: number) {
    const product = this.productRepository.findOneBy({ id: id });

    return product;
  }

  async update(id: number, updateProductDto: UpdateProductDto) {
    const product = await this.productRepository.findOneBy({ id: id });
    const updatedUser = {
      ...product,
      ...updateProductDto,
    };
    return this.productRepository.save(updatedUser);
  }

  async remove(id: number) {
    const product = await this.productRepository.findOneBy({ id: id });

    return this.productRepository.remove(product);
  }
}
